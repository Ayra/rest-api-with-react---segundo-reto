import React, { Component } from 'react';
import { Row, Col, Input, Select, Button, Card } from 'antd';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';


import './App.css';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      events: [],
      categories: [],
      nombre: '',
      requestFailed: false,
      categoria: 'Seleccione...'
    };
    this.handleName = this.handleName.bind(this);
  //  this.handleCategory = this.handleCategory.bind(this);
  }


  componentDidMount() {

    fetch('https://www.eventbriteapi.com/v3/events/?token=DHAQ5TXGQKOOI7WTPTXY&expand=category')
      .then(response => {
        if (!response.ok) {
          throw Error("Network Request Failed");
        }
        return response
      })
      .then(d => d.json())
      .then(d => {
        this.setState({
          events: d.events
        })

        fetch('https://www.eventbriteapi.com/v3/categories/?token=DHAQ5TXGQKOOI7WTPTXY')
          .then(data => data.json())
          .then(data => {
            this.setState({
              categories: data.categories
            })
          })
      }, () => {
        this.setState({
          requestFailed: true
        })
      })

  }

  handleName(e) {
    this.setState({
      nombre: e.target.value
    })
    //console.log(e.target.value);
  }

  handleCategory(e) {
      this.setState({
        categoria: e
      })
  }

  render() {
    const Option = Select.Option;
    const { events } = this.state;
    const { Meta } = Card;
    //const array = JSON.parse(this.state.data.categories);
    console.log("arraay", this.state.events);
    console.log("cateogires", this.state.categories);
    return (

      <div className="App">
        <Row type="flex" justify="center">
          <h1 className="maintitle">Busca tu evento por nombre o categoria</h1>
        </Row>
        <Row type="flex" justify="center">
          <Col className="columna" span={5}>
            <Input
              type="text"
              name="nombre"
              placeholder="Nombre evento"
              onChange={this.handleName}
              value={this.state.nombre} />
          </Col>
          <Col className="columna" span={5}>
            <Select
              defaultValue={this.state.categoria}
              style={{ width: 300 }}
              onChange={this.handleCategory.bind(this)}>
              {
                this.state.categories.map((category, indice) => {
                  const { id, name } = category;
                  return (
                    <Option value={id} key={indice}> {name}  </Option>
                  )
                })
              }
            </Select>
          </Col>
          <Col className="columna" span={5}>
            <Button type="primary" block>Buscar</Button>
          </Col>
        </Row>
        <Row type="flex" justify="center" className="tarjetas">
          {
            events.map((event, i) => {
              const { created, name, logo, category } = event;
              if (name.text.toUpperCase().indexOf((this.state.nombre).toUpperCase()) > -1) {
                if (category != null) {
                  {console.log(this.state.categoria+ ":", category.name)}
                  if (category.id == this.state.categoria || this.state.categoria == "Seleccione...") {
                    return (
                      <Col className="columna" span={5} key={i}>
                        <Card
                          hoverable
                          style={{ width: 300 }}
                          cover={<img alt="example" src={logo != null ? logo.url : "https://img.evbuc.com/https%3A%2F%2Fcdn.evbuc.com%2Fimages%2F58764065%2F225810853155%2F1%2Foriginal.20190319-191829?h=200&w=450&auto=compress&rect=0%2C0%2C904%2C452&s=e87c82f22242407074bf34e4b1536981"} />}
                        >
                          <Meta
                            title={name.text}
                            description={category != null ? category.name : "Without Category"}
                          />
                        </Card>
                      </Col>
                    );
                  }
                }


              }

            })
          }


        </Row>

      </div>
    );
  }
}

export default App;
